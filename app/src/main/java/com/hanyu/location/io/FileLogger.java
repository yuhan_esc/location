package com.hanyu.location.io;

import android.content.Context;
import android.os.Environment;

import com.hanyu.location.util.LogUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileLogger {
    public static final String TAG = "LocationDemo.FileLogger";
    private static final String FILE_PREFIX = "nmea_log";

    private Context mContext;

    private final Object mFileLock = new Object();
    private BufferedWriter mFileWriter;
    private File mFile;

    public FileLogger(Context context) {
        mContext = context;
    }

    public void createNmeaLogFile() {
        synchronized (mFileLock) {
            File baseDirectory;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                baseDirectory = new File(mContext.getExternalFilesDir(null), FILE_PREFIX);
                baseDirectory.mkdirs();
            }else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                LogUtil.e(TAG,"Cannot write to external storage.");
                return;
            } else {
                LogUtil.e(TAG,"Cannot read external storage.");
                return;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
            Date now = new Date();
            String fileName = String.format("%s_%s.txt", FILE_PREFIX,formatter.format(now));
            File currentFile = new File(baseDirectory,fileName);
            String currentFilePath = currentFile.getAbsolutePath();
            BufferedWriter writer = null;
            if (currentFile.exists()) {
                try {
                    //当天文件存在，追加写入
                    writer = new BufferedWriter(new FileWriter(currentFile, true));
                }catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }else {
                try {
                    //新的一天，新建一个文件
                    writer = new BufferedWriter(new FileWriter(currentFile));
                }catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }

            mFile = currentFile;
            mFileWriter = writer;
            LogUtil.toast(mContext, "NMea语句保存在" + currentFilePath);
        }
    }

    public void close() {
        if (mFileWriter != null) {
            try {
                mFileWriter.flush();
                mFileWriter.close();
                mFileWriter = null;
                LogUtil.toast(mContext, "NMea语句停止记录");
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeNmeaToFile(String nmea) {
        synchronized (mFileLock) {
            if (mFileWriter == null) {
                return;
            }
            try {
                mFileWriter.write(nmea);
                mFileWriter.newLine();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

}

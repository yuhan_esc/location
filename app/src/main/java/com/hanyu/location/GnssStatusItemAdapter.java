package com.hanyu.location;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class GnssStatusItemAdapter extends RecyclerView.Adapter<GnssStatusItemAdapter.ViewHolder>{

    private Context mContext;
    private LayoutInflater inflater;
    private List<GnssStatusItem> mGnssStatusItems;

    public GnssStatusItemAdapter(Context context, List<GnssStatusItem> gnssStatusItems) {
        this.mContext = context;
        this.mGnssStatusItems = gnssStatusItems;
        inflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = inflater.inflate(R.layout.gnss_status_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GnssStatusItem gnssStatusItem = mGnssStatusItems.get(position);
        holder.textView_id.setText(gnssStatusItem.getSatelliteID());
        holder.textView_gnss.setText(gnssStatusItem.getConstellationType());
        holder.textView_cf.setText(gnssStatusItem.getCarrierFrequencyHz());
        holder.textView_cn0.setText(gnssStatusItem.getCn0DbHz());
        holder.textView_used.setText(gnssStatusItem.getUsedInFix());
        holder.textView_elevation_degrees.setText(gnssStatusItem.getElevationDegrees());
        holder.textView_azimuth_degrees.setText(gnssStatusItem.getAzimuthDegrees());
    }

    @Override
    public int getItemCount() {
        return mGnssStatusItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        View gnssStatusItemView;
        private TextView textView_id,textView_gnss,textView_cf,textView_cn0,textView_used,textView_elevation_degrees,textView_azimuth_degrees;

        public ViewHolder(View view){
            super(view);
            this.gnssStatusItemView = view;
            textView_id = view.findViewById(R.id.gnss_status_id);
            textView_gnss = view.findViewById(R.id.gnss_status_gnss);
            textView_cf = view.findViewById(R.id.gnss_status_cf);
            textView_cn0 = view.findViewById(R.id.gnss_status_cn0);
            textView_used = view.findViewById(R.id.gnss_status_used);
            textView_elevation_degrees = view.findViewById(R.id.gnss_status_elevation_degrees);
            textView_azimuth_degrees = view.findViewById(R.id.gnss_status_azimuth_degrees);
        }
    }
}

package com.hanyu.location.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/*
 * 定制的日志打印类
 * */
public class LogUtil {

    private static final int VERBOSE = 1;
    private static final int DEBUG = 2;
    private static final int INFO = 3;
    private static final int WARN = 4;
    private static final int ERROR = 5;

    private LogUtil(){}
    //定义日志打印级别
    private static final int currentLevel = VERBOSE;

    public static void v(String tag, String msg) {
        if (currentLevel <= VERBOSE){
            Log.v(tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (currentLevel <= DEBUG){
            Log.d(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (currentLevel <= INFO){
            Log.i(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (currentLevel <= WARN){
            Log.w(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (currentLevel <= ERROR){
            Log.e(tag, msg);
        }
    }

    public static void toast(Context context, CharSequence str) {
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }
}

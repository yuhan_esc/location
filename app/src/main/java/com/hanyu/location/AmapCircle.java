package com.hanyu.location;

import android.graphics.Color;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.model.Circle;
import com.amap.api.maps2d.model.CircleOptions;
import com.amap.api.maps2d.model.LatLng;

public class AmapCircle {
    private Circle mCircle;
    private LatLng mLatLng;
    private double mRadiusMeters;
    private AMap aMap;

    public AmapCircle(LatLng center, double radiusMeters, AMap map) {
        this.mLatLng = center;
        mRadiusMeters = radiusMeters;
        aMap = map;
    }

    public AmapCircle(AMap map) {
        aMap = map;
    }

    public void addCircle(LatLng center, double radiusMeters) {
        this.mLatLng = center;
        this.mRadiusMeters = radiusMeters;
        mCircle = aMap.addCircle(new CircleOptions()
                .center(mLatLng)
                .radius(mRadiusMeters)
                .strokeWidth(1f)
                .strokeColor(Color.BLUE)
                .fillColor(Color.BLUE));
    }

    public void remove() {
        if (mCircle != null) {
            mCircle.remove();
        }
    }
}

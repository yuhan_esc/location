package com.hanyu.location;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Point implements Parcelable {

    private double latitude,longitude;

    public Point(double lat, double lng) {
        this.latitude = lat;
        this.longitude = lng;
    }

    protected Point (Parcel source) {
        latitude = source.readDouble();
        longitude = source.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Point> CREATOR = new Creator<Point>() {
        @Override
        public Point createFromParcel(Parcel source) {
            return new Point(source);
        }

        @Override
        public Point[] newArray(int size) {
            return new Point[size];
        }
    } ;

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @NonNull
    @Override
    public String toString() {
        return "(" + latitude + ", " + longitude + ")";
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double lat) {
        this.latitude = lat;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double lng) {
        this.longitude = lng;
    }
}

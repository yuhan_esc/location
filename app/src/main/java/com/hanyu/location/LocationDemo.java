package com.hanyu.location;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.OnNmeaMessageListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdate;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.UiSettings;
import com.amap.api.maps2d.model.LatLng;
import com.hanyu.location.io.FileLogger;
import com.hanyu.location.util.CoordinateTransformUtils;
import com.hanyu.location.util.LogUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocationDemo extends AppCompatActivity
        implements LocationListener{

    private static final String TAG = "LocationDemo";
    private static final int PERMISSION_CODE_ACCESS_LOCATION = 100;
    private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 101;
    private static final String INJECT_XTRA_DATA = "force_xtra_injection";
    private static final String INJECT_PSDS_DATA = "force_psds_injection";
    private static final String INJECT_TIME_DATA = "force_time_injection";
    private static final String CLEAR_AIDING_DATA = "delete_aiding_data";
    private Context mContext;
    private LocationManager mLocationManager;
    private LocationProvider mProvider;
    private FileLogger mFileLogger;
    AlertDialog mPermissionDialog, mLocationSwitchDialog;
    String mPackageName = "com.hanyu.location";
    List<String> mPermissionList = new ArrayList<>();
    TextView textView_Location;
    Switch switch_gps;
    boolean mStarted,mRecordNmea;
    int mSatelliteCount;

    private List<GnssStatusItem> gnssStatusItemsArrayList = new ArrayList<>();
    private GnssStatusItemAdapter gnssStatusItemAdapter;
    private RecyclerView recyclerView;

    //Amap
    private MapView mMapView = null;
    private AMap aMap = null;
    private UiSettings mUiSettings;
    private Bundle tmp_Bundle;
    private AmapCircle amapCircle;

    //gnss status and listener
    private GnssStatus mGnssStatus;
    private GnssStatus.Callback mGnssStatusListener;
    private OnNmeaMessageListener mOnNmeaMessageListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogUtil.v(TAG, "LocationDemo.onCreate()");
        mContext = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tmp_Bundle = savedInstanceState;
        instanceView();
        initPermissions();
        startWork();

    }

    @Override
    protected void onPause() {
        LogUtil.v(TAG, "LocationDemo.onPause()");
        super.onPause();
    }

    @Override
    protected void onResume() {
        LogUtil.v(TAG, "LocationDemo.onResume()");
        super.onResume();
    }

    @Override
    protected void onStop() {
        LogUtil.v(TAG, "LocationDemo.onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        LogUtil.v(TAG, "LocationDemo.onDestroy()");
        if (mRecordNmea) {
            mRecordNmea = false;
            mFileLogger.close();
        }
        gpsStop();
        removeGnssStatusListener();
        removeNmeaListener();
        super.onDestroy();
    }

    private void initPermissions() {
        mPermissionList.clear();
        switch (Build.VERSION.SDK_INT - Build.VERSION_CODES.O){
            case 0://API 26 Android 8
            case 1://API 27 Android 8.1
            case 2://API28 Android 9
                //only request permission: ACCESS_FINE_LOCATION
                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    mPermissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
                }else {
                    return;
                }
                if (mPermissionList.size() == 1){
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSION_CODE_ACCESS_LOCATION);
                    return;
                }
                break;
            case 3://API29 Android 10
            case 4://API30 Android 11
            case 5:
                //request permissions:ACCESS_FINE_LOCATION and ACCESS_BACKGROUND_LOCATION
                int mPermSize = 0;
                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    mPermissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
                }else {
                    mPermSize = mPermSize + 1;
                }
/*                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    mPermissionList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                }else {
                    mPermSize = mPermSize + 2;
                }
                switch (mPermSize){
                    case 0:
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                                PERMISSION_CODE_ACCESS_LOCATION);
                        break;
                    case 1:
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                                PERMISSION_CODE_ACCESS_LOCATION);
                        break;
                    default:
                }*/
            default:
                //不支持其他版本的系统
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean hasPermissionDismiss = false;//有权限没有通过
        if (requestCode == PERMISSION_CODE_ACCESS_LOCATION) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == -1) {
                    hasPermissionDismiss = true;
                }
            }
            //如果有权限没有被允许
            if (hasPermissionDismiss) {
                //跳转到系统设置权限页面，或者直接关闭页面，不让他继续访问
                showPermissionDialog();
            }
        }
    }

    private void showPermissionDialog() {
        if (mPermissionDialog == null) {
            mPermissionDialog = new AlertDialog.Builder(this)
                    .setMessage("已禁用权限，请手动授予")
                    .setPositiveButton("设置", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mPermissionDialog.cancel();

                            Uri packageURI = Uri.parse("package:" + mPackageName);
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //关闭页面或者做其他操作
                            mPermissionDialog.cancel();

                        }
                    })
                    .create();
        }
        mPermissionDialog.show();
    }

    private void instanceView() {
        textView_Location = findViewById(R.id.textview_location);
        switch_gps = findViewById(R.id.menu_gps_switch_item);
        //加载Map
        mMapView = findViewById(R.id.map_obtainPoint);
        mMapView.onCreate(tmp_Bundle);
        if (aMap == null){
            aMap = mMapView.getMap();
            mUiSettings = aMap.getUiSettings();
        }
        //load Amap
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setScaleControlsEnabled(true);

        //加载recyclerView
        recyclerView = findViewById(R.id.recyclerView_gnssStatus);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
        gnssStatusItemAdapter = new GnssStatusItemAdapter(mContext, gnssStatusItemsArrayList);
        recyclerView.setAdapter(gnssStatusItemAdapter);
        gnssStatusItemAdapter.notifyDataSetChanged();
    }

    private void startWork() {
        mLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        //只选用GPS定位
        mProvider = mLocationManager.getProvider(LocationManager.GPS_PROVIDER);
        mFileLogger = new FileLogger(mContext);
        mRecordNmea = false;
        checkGpsProvider();

        addGnssStatusListener();

        addNmeaListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_locationdemo, menu);
        initGpsSwitch(menu);
        //返回true表示将创建的菜单显示出来
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //处理按钮
        switch (item.getItemId()) {
            case R.id.menu_inject_xtra_data:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (mLocationManager.sendExtraCommand(mProvider.getName(), INJECT_PSDS_DATA, null)){
                        LogUtil.toast(mContext,"成功导入XTRA数据");
                    }else {
                        LogUtil.toast(mContext,"导入XTRA数据失败");
                    }
                }else {
                    if (mLocationManager.sendExtraCommand(mProvider.getName(), INJECT_XTRA_DATA, null)){
                        LogUtil.toast(mContext,"成功导入XTRA数据");
                    }else {
                        LogUtil.toast(mContext,"导入XTRA数据失败");
                    }
                }
                break;
            case R.id.menu_inject_time_data:
                if (mLocationManager.sendExtraCommand(mProvider.getName(), INJECT_TIME_DATA, null)) {
                    LogUtil.toast(mContext,"成功导入时间数据");
                }else {
                    LogUtil.toast(mContext,"导入时间数据失败");
                }
                break;
            case R.id.menu_clear_aiding_data:
                //GPS开关如果打开，先关闭
                boolean switch_clear = false;
                if (mStarted && switch_gps.isChecked()) {
                    switch_gps.setChecked(false);
                    gpsStop();
                    switch_clear = true;
                }
                if (mLocationManager.sendExtraCommand(mProvider.getName(), CLEAR_AIDING_DATA, null)) {
                    LogUtil.toast(mContext,"成功清除辅助数据");
                    textView_Location.setText("location");
                    gnssStatusItemsArrayList.clear();
                    gnssStatusItemAdapter.notifyDataSetChanged();
                    if (switch_clear) {
                        switch_gps.setChecked(true);
                        gpsStart();
                    }

                } else {
                    LogUtil.toast(mContext,"清除辅助数据失败");
                }
                break;
            case R.id.menu_nmea_log:
                //check permission
                if (ContextCompat.checkSelfPermission(mContext,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_WRITE_EXTERNAL_STORAGE);
                }
                mFileLogger.createNmeaLogFile();
                mRecordNmea = true;
                break;
            default:
        }
        return true;
    }

    private void initGpsSwitch(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_gps_switch_item);
        if (item != null) {
            switch_gps = MenuItemCompat.getActionView(item).findViewById(R.id.gps_switch);
            if (switch_gps != null) {
                // Initialize state of GPS switch before we set the listener, so we don't double-trigger start or stop
                switch_gps.setChecked(mStarted);

                // Set up listener for GPS on/off switch
                switch_gps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Turn GPS on or off
                        if (!switch_gps.isChecked() && mStarted) {
                            //LogUtil.toast(mContext, "stop Navigation");
                            gpsStop();
                        } else {
                            if (switch_gps.isChecked() && !mStarted) {
                                //LogUtil.toast(mContext, "start Navigation");
                                gpsStart();
                            }
                        }
                    }
                });
            }
        }
    }

    private void checkGpsProvider() {
        //判断位置导航开关是否打开
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            if (mLocationSwitchDialog == null){
                mLocationSwitchDialog = new AlertDialog.Builder(this)
                        .setMessage("位置导航开关已关闭，请用户手动打开。")
                        .setPositiveButton("设置", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mLocationSwitchDialog.cancel();
                                Uri packageURI = Uri.parse("package:" + mPackageName);
                                Intent intent = new Intent();
                                intent.setClassName("com.android.settings",
                                        "com.android.settings.Settings$LocationSettingsActivity");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //关闭页面或者做其他操作
                                mLocationSwitchDialog.cancel();

                            }
                        })
                        .create();
            }
            mLocationSwitchDialog.show();
        }
    }

    @SuppressLint("MissingPermission")
    private void addGnssStatusListener() {
        mGnssStatusListener = new GnssStatus.Callback() {
            @Override
            public void onStarted() {
                super.onStarted();
                //LogUtil.toast(mContext, "start gnss engine");
            }

            @Override
            public void onStopped() {
                super.onStopped();
                //LogUtil.toast(mContext, "stop gnss engine");
            }

            @Override
            public void onFirstFix(int ttffMillis) {
                super.onFirstFix(ttffMillis);
                LogUtil.toast(mContext, "TTFF: " + ttffMillis/1000 + "s");
            }

            @Override
            public void onSatelliteStatusChanged(GnssStatus status) {
                LogUtil.v(TAG, "onSatelliteStatusChanged.");
                super.onSatelliteStatusChanged(status);
                //清空ArrayList
                gnssStatusItemsArrayList.clear();
                gnssStatusItemsArrayList.add(new GnssStatusItem(
                        "ID",
                        "GNss",
                        "CF",
                        "C/N0",
                        "Used",
                        "高度角",
                        "方位角"));

                //追加显示卫星状态
                mGnssStatus = status;
                mSatelliteCount = mGnssStatus.getSatelliteCount();
                for (int i =0; i < mSatelliteCount ; i++) {
                    //GNSS
                    String str_constellation_type = "UNKNOWN";
                    switch(mGnssStatus.getConstellationType(i)) {
                        case 1:
                            str_constellation_type = "GPS    ";
                            break;
                        case 2:
                            str_constellation_type = "SBAS   ";
                            break;
                        case 3:
                            str_constellation_type = "GLONASS";
                            break;
                        case 4:
                            str_constellation_type = "QZSS   ";
                            break;
                        case 5:
                            str_constellation_type = "BEIDOU ";
                            break;
                        case 6:
                            str_constellation_type = "GALILEO";
                            break;
                        case 7:
                            str_constellation_type = "IRNSS  ";
                            break;
                        default:
                            str_constellation_type = "UNKNOWN";
                    }
                    GnssStatusItem gnssStatusItem = new GnssStatusItem(
                            String.format(Locale.CHINA,"%d",mGnssStatus.getSvid(i)),
                            str_constellation_type,
                            String.format(Locale.CHINA,"%e",mGnssStatus.getCarrierFrequencyHz(i)),
                            String.format(Locale.CHINA,"%3.1f",mGnssStatus.getCn0DbHz(i)),
                            String.format(Locale.CHINA,"%b",mGnssStatus.usedInFix(i)),
                            String.format(Locale.CHINA,"%3.1f",mGnssStatus.getElevationDegrees(i)),
                            String.format(Locale.CHINA,"%3.1f",mGnssStatus.getAzimuthDegrees(i)));

                    gnssStatusItemsArrayList.add(gnssStatusItem);
                }
                gnssStatusItemAdapter.notifyDataSetChanged();
            }
        };
        mLocationManager.registerGnssStatusCallback(mGnssStatusListener);
    }

    private void removeGnssStatusListener() {
        if (mLocationManager != null) {
            mLocationManager.unregisterGnssStatusCallback(mGnssStatusListener);
        }
    }

    @SuppressLint("MissingPermission")
    private void addNmeaListener() {
        if (mOnNmeaMessageListener == null) {
            mOnNmeaMessageListener = new OnNmeaMessageListener() {
                @Override
                public void onNmeaMessage(String message, long timestamp) {
                    LogUtil.v(TAG, String.format(Locale.CHINA, "%d,%s", timestamp,message));
                    if (mRecordNmea){
                        mFileLogger.writeNmeaToFile(message);
                    }
                }
            };
        }
        mLocationManager.addNmeaListener(mOnNmeaMessageListener);
    }

    private void removeNmeaListener() {
        if (mOnNmeaMessageListener != null){
            mLocationManager.removeNmeaListener(mOnNmeaMessageListener);
        }
    }

    @SuppressLint("MissingPermission")
    private synchronized void gpsStart() {
        LogUtil.v(TAG, "gpsStart()");
        if (mLocationManager == null || mProvider == null){
            return;
        }
        if (!mStarted){
            mStarted = true;
            //requestLocationUpdates
            textView_Location.setText("location");
            gnssStatusItemsArrayList.clear();
            gnssStatusItemAdapter.notifyDataSetChanged();
            aMap.clear();
            mLocationManager.requestLocationUpdates(mProvider.getName(), 1000l, 10f, this);

        }
    }

    @SuppressLint("MissingPermission")
    private synchronized void gpsStop() {
        LogUtil.v(TAG, "gpsStop()");
        if (mStarted){
            mStarted = false;
            //removeUpdates
            mLocationManager.removeUpdates(this);
            if (mRecordNmea) {
                mRecordNmea = false;
                mFileLogger.close();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        textView_Location.setText("(" + location.getLatitude() +","
                + location.getLongitude() + "),"
                + location.getAccuracy());
        //show location on Amap
        //wgs84-->gcj02
        Point aMapPoint = CoordinateTransformUtils.wgs84ToGcj02(location.getLatitude(), location.getLongitude());
        if (amapCircle == null) {
            amapCircle = new AmapCircle(aMap);
        }
        //show a point
        LatLng aMapLatLng = new LatLng(aMapPoint.getLatitude(), aMapPoint.getLongitude());
        amapCircle.addCircle(aMapLatLng, 1);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(aMapLatLng, 19);
        aMap.moveCamera(cameraUpdate);
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        checkGpsProvider();
    }
}

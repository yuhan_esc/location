package com.hanyu.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class GnssStatusItem implements Parcelable {

    private String mSatelliteID;
    private String mConstellationType,mUsedInFix;
    private String mCarrierFrequencyHz,mCn0DbHz,mElevationDegrees,mAzimuthDegrees;

    public static final Creator<GnssStatusItem> CREATOR = new Creator<GnssStatusItem>() {
        @Override
        public GnssStatusItem createFromParcel(Parcel source) {
            return new GnssStatusItem(source);
        }

        @Override
        public GnssStatusItem[] newArray(int size) {
            return new GnssStatusItem[size];
        }
    };

    public GnssStatusItem(String satelliteID, String constellationType, String carrierFrequencyHz, String cn0DbHz,
                          String usedInFix, String elevationDegrees,String azimuthDegrees) {
        mSatelliteID = satelliteID;
        mConstellationType = constellationType;
        mCarrierFrequencyHz = carrierFrequencyHz;
        mCn0DbHz = cn0DbHz;
        mUsedInFix = usedInFix;
        mElevationDegrees = elevationDegrees;
        mAzimuthDegrees = azimuthDegrees;
    }

    protected GnssStatusItem(Parcel source) {
        mSatelliteID = source.readString();
        mConstellationType = source.readString();
        mCarrierFrequencyHz = source.readString();
        mCn0DbHz = source.readString();
        mUsedInFix = source.readString();
        mElevationDegrees = source.readString();
        mAzimuthDegrees = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSatelliteID);
        dest.writeString(mConstellationType);
        dest.writeString(mCarrierFrequencyHz);
        dest.writeString(mCn0DbHz);
        dest.writeString(mUsedInFix);
        dest.writeString(mElevationDegrees);
        dest.writeString(mAzimuthDegrees);
    }

    public String getSatelliteID() {
        return mSatelliteID;
    }

    public void setSatelliteID(String num) {
        mSatelliteID = num;
    }

    public String getConstellationType() {
        return mConstellationType;
    }

    public void setConstellationType(String str) {
        mConstellationType = str;
    }

    public String getCarrierFrequencyHz() {
        return mCarrierFrequencyHz;
    }

    public void setCarrierFrequencyHz(String num) {
        mCarrierFrequencyHz = num;
    }

    public String getCn0DbHz() {
        return mCn0DbHz;
    }

    public void setCn0DbHz(String num) {
        mCn0DbHz = num;
    }

    public String getUsedInFix() {
        return mUsedInFix;
    }

    public void setUsedInFix(String str) {
        mUsedInFix = str;
    }

    public String getElevationDegrees() {
        return mElevationDegrees;
    }

    public void setElevationDegrees(String num) {
        mElevationDegrees = num;
    }

    public String getAzimuthDegrees() {
        return mAzimuthDegrees;
    }

    public void setAzimuthDegrees(String num) {
        mAzimuthDegrees = num;
    }

}
